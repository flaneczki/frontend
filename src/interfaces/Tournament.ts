export interface Tournament {
  tournament_name: string;
  location: string;
  tournament_date: Date;
}
