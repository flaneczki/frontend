export interface Player {
  name: string;
  city: string;
  photo: string;
  created_at: Date;
  updated_at: Date;
}
