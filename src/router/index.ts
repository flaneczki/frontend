import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/tournaments',
    name: 'Tournaments',
    component: () => { return import(/* webpackChunkName: "tournaments" */ '../views/Tournaments.vue'); },
  },
  {
    path: '/players',
    name: 'Players',
    component: () => { return import(/* webpackChunkName: "players" */ '../views/Players.vue'); },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
