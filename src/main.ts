import Vue from 'vue';
import VueFilterDateParse from 'vue-filter-date-parse';
import VueFilterDateFormat from 'vue-filter-date-format';
import App from './App.vue';
import './assets/styles/index.css';
import './registerServiceWorker';
import router from './router';

Vue.config.productionTip = false;

Vue.use(VueFilterDateParse);
Vue.use(VueFilterDateFormat);

new Vue({
  router,
  render: (h) => { return h(App); },
}).$mount('#app');
