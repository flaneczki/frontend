import Api from '@/services/Api';

export default {
  getPlayers() {
    return Api().get('/api/v1/players');
  },
};
