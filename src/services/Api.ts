import axios from 'axios';

export default () => {
  return axios.create({
    baseURL: 'http://frelia.org:3000',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
};
