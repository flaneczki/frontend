import Api from '@/services/Api';

export default {
  getTournaments() {
    return Api().get('/api/v1/tournaments');
  },
};
